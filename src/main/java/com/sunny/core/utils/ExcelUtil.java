package com.sunny.core.utils;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author: fxy
 * @date: 2019/04/23
 */
public class ExcelUtil {

    /**
     * 创建单行excel模板
     * @param sheetName
     * @param title
     * @param out
     */
    public static void createTemplate(String sheetName, String[] title, OutputStream out) {
        int length = title.length;
        //创建HSSFWorkbook对象
        HSSFWorkbook wb = new HSSFWorkbook();
        //创建HSSFSheet对象    
        HSSFSheet sheet = wb.createSheet(sheetName);
        //这是设置整个列表的高度高
        sheet.setDefaultRowHeightInPoints(20);
        //设置整个列表的宽度//
        sheet.setDefaultColumnWidth(20);
        //合并单元格，数字代表的依次为起始行，截至行，起始列， 截至列
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, length));
        //创建样式
        HSSFCellStyle cellStyle = wb.createCellStyle();
        //左右居中
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        //上下居中
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        //设置前景色
        cellStyle.setFillForegroundColor(IndexedColors.RED.index);
        //设置背景颜色
        cellStyle.setFillBackgroundColor(IndexedColors.LIGHT_YELLOW.index);
        //设置底边边框，同理可设置其它边框
        cellStyle.setBorderBottom(BorderStyle.SLANTED_DASH_DOT);
        // 设置单元格底部的边框颜色
        cellStyle.setBottomBorderColor(IndexedColors.DARK_RED.index);
        //创建行元素对象，数字表示行数，0为第一行
        HSSFRow row = sheet.createRow(0);
        for (int i = 0; i < length; i++) {
            //创建单元格对象，为每一个单元格
            HSSFCell cell = row.createCell(i);
            //针对当前列设置行高
            row.setHeightInPoints(50);
            //设置当前单元格显示内容
            cell.setCellValue(title[i]);
            //设置当前单元格样式
            cell.setCellStyle(cellStyle);
        }
        //输出Excel文件
        try {
            wb.write(out);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(wb != null){
                try {
                    wb.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(out != null){
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    
    


}
