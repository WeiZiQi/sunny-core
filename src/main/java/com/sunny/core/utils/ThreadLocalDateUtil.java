package com.sunny.core.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 线程安全日期格式化
 * @author fxy
 * @date 2018/6/1
 */
public class ThreadLocalDateUtil {
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static ThreadLocal<DateFormat> threadLocal = new ThreadLocal<>();

    public static DateFormat getDateFormat() {
        return getDateFormat(DATE_FORMAT);
    }

    public static DateFormat getDateFormat(String pattern) {
        DateFormat df = threadLocal.get();
        if(df==null){
            df = new SimpleDateFormat(pattern);
            threadLocal.set(df);
        }
        return df;
    }

    public static String formatDate(Date date) {
        if(date == null){
            return null;
        }
        String format = getDateFormat().format(date);
        threadLocal.remove();
        return format;
    }

    public static String formatDate(Date date,String pattern) {
        if(date == null){
            return null;
        }
        String format = getDateFormat(pattern).format(date);
        threadLocal.remove();
        return format;
    }


    public static Date parse(String strDate) {
        if(StringUtils.isEmpty(strDate)){
            return null;
        }
        Date parse = null;
        try {
            parse = getDateFormat().parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        threadLocal.remove();
        return parse;
    }

}

