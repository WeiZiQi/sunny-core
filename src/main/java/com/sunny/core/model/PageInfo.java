package com.sunny.core.model;

import java.io.Serializable;
import java.util.List;

/**
 * 分页信息
 *
 * @param <T>
 * @author fengxiangyang
 * @date 2018/11/30
 */
public class PageInfo<T> implements Serializable {
    /**
     * 第几页
     */
    private int pageNo;
    /**
     * 每页的数量
     */
    private int pageSize;
    /**
     * 数据总数
     */
    private int total;
    /**
     * 数据集
     */
    private List<T> dataList;

    public PageInfo() {
    }

    public PageInfo(int pageNo, int pageSize) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<T> getDataList() {
        return dataList;
    }

    public void setDataList(List<T> dataList) {
        this.dataList = dataList;
    }

    /**
     * 获取总页数
     *
     * @return
     */
    public Integer getPageTotal() {
        return (total + pageSize - 1) / pageSize;
    }
}
