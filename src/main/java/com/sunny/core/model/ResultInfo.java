package com.sunny.core.model;

import com.sunny.core.constants.ResultConstant;

import java.io.Serializable;

/**
 * 返回结果
 *
 * @param <T>
 * @author fengxiangyang
 * @date 2018/11/30
 */
public class ResultInfo<T> implements Serializable {
    private int code;
    private String msg;
    private T data;

    public ResultInfo() {
    }

    public ResultInfo(int code) {
        this.code = code;
    }

    public ResultInfo(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    /**
     * 是否成功
     *
     * @return
     */
    public boolean isSuccess() {
        return ResultConstant.CODE_SUCCESS == code;
    }
}
