package com.sunny.core.utils.excel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author: fxy
 * @date: 2019/04/24
 */
public class ExcelReaderUtil {
    private List<Map<String, String>> data = new ArrayList<>();
    /**
     * 每获取一条记录，即打印
     * 在flume里每获取一条记录即发送，而不必缓存起来，可以大大减少内存的消耗，这里主要是针对flume读取大数据量excel来说的
     * @param sheetName
     * @param sheetIndex
     * @param curRow  当前行
     * @param cellList 当前行内cell集合
     */
    public static void sendRows(String sheetName, int sheetIndex, int curRow, List<String> cellList) {
        StringBuffer oneLineSb = new StringBuffer();
        oneLineSb.append("--");
        oneLineSb.append("sheet" + sheetIndex);
        oneLineSb.append("::" + sheetName);//加上sheet名
        oneLineSb.append("--");
        oneLineSb.append("row" + curRow);
        oneLineSb.append("::");
        for (String cell : cellList) {
            oneLineSb.append(cell.trim());
            oneLineSb.append("|");
        }
        String oneLine = oneLineSb.toString();
        if (oneLine.endsWith("|")) {
            oneLine = oneLine.substring(0, oneLine.lastIndexOf("|"));
        }// 去除最后一个分隔符

        System.out.println(oneLine);
    }

    public static void readExcel(String fileName) throws Exception {
        ExcelXlsxReader excelXlsxReader = new ExcelXlsxReader();
        int process = excelXlsxReader.process(fileName);
    }
}